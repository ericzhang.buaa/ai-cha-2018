from keras.models import Model
from keras.layers import *


class BiLSTMWithSimpleAttention(object):
    def __init__(self, word_index, embedding_matrix, embedding_size=300, sentence_length=500, hidden_dim=200):
        self.word_index = word_index
        self.embedding_matrix = embedding_matrix
        self.embedding_size = embedding_size
        self.sentence_length = sentence_length
        self.hidden_dim = hidden_dim

    def build(self):
        input = Input((self.sentence_length,))
        embedding = Embedding(len(self.word_index) + 1, self.embedding_size, weights=[
                              self.embedding_matrix], trainable=False, input_length=self.sentence_length)(input)
        embedding = SpatialDropout1D(0.2)(embedding)
        if len(K.tensorflow_backend._get_available_gpus()) > 0:
            lstm = Bidirectional(
                CuDNNLSTM(self.hidden_dim, return_sequences=True))(embedding)
        else:
            lstm = Bidirectional(
                LSTM(self.hidden_dim, return_sequences=True))(embedding)
        lstm = Dropout(0.2)(lstm)
        if len(K.tensorflow_backend._get_available_gpus()) > 0:
            lstm = Bidirectional(
                CuDNNLSTM(self.hidden_dim, return_sequences=True))(lstm)
        else:
            lstm = Bidirectional(
                LSTM(self.hidden_dim, return_sequences=True))(embedding)
        lstm = Dropout(0.2)(lstm)
        att_encode = SimpleAttentionLayer(reg=0.)(lstm)
        avg_encode = GlobalAveragePooling1D()(lstm)
        max_encode = GlobalMaxPooling1D()(lstm)
        encode = concatenate([att_encode, avg_encode, max_encode])
        dense = Dense(4, activation='softmax',
                      kernel_regularizer=l2(0.001))(encode)
        model = Model(inputs=input, outputs=dense)

        return model
