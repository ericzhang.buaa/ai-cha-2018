from keras import backend as K
import pandas as pd
import numpy as np
import os
from common import preprocess as pp
from keras.callbacks import Callback, TensorBoard, ModelCheckpoint
from sklearn.metrics import classification_report, f1_score
from keras.optimizers import Adam
from layers.simple_attention import SimpleAttentionLayer
import tensorflow as tf
from keras.regularizers import l2
from bilstm_with_simple_attention import BiLSTMWithSimpleAttention


# 显示可用GPU信息
print('GPU Info: ', K.tensorflow_backend._get_available_gpus())

EMBEDDING_SIZE = 300
PADDING_SIZE = 500
data_dir = os.path.join(os.path.dirname(__file__), 'data')

train_data = pd.read_csv('./data/sentiment_analysis_trainingset.csv', sep=',')
train_data['content'] = train_data['content'].str.strip('"')
print('Train set size: ', len(train_data))

valid_data = pd.read_csv(
    './data/sentiment_analysis_validationset.csv', sep=',')
valid_data['content'] = valid_data['content'].str.strip('"')
print('Validation set size: ', len(valid_data))

testa_data = pd.read_csv('./data/sentiment_analysis_testa.csv', sep=',')
testa_data['content'] = testa_data['content'].str.strip('"')
print('TestA set size: ', len(testa_data))

train_sentences = np.load(os.path.join(data_dir, 'train_coded_sentences.npy'))
valid_sentences = np.load(os.path.join(data_dir, 'valid_coded_sentences.npy'))
testa_sentences = np.load(os.path.join(data_dir, 'testa_coded_sentences.npy'))
word_index = np.load(os.path.join(data_dir, 'word_index.npy')).item()
embedding_matrix = np.load(os.path.join(data_dir, 'embedding_matrix.npy'))
y_train = np.load(os.path.join(data_dir, 'y_train.npy'))
y_valid = np.load(os.path.join(data_dir, 'y_valid.npy'))

aspects = aspects = train_data.columns[2:]
aspect = None


class MetricMacroF1(Callback):
    def on_train_begin(self, logs={}):
        self.best_f1 = 0.

    def on_epoch_begin(self, epoch, logs={}):
        pass

    def on_epoch_end(self, epoch, logs={}):
        y_prob = model.predict(self.validation_data[0])
        val_predicts = [y.argmax(axis=-1) for y in y_prob]
        val_target = pp.decode_one_hot_label(self.validation_data[1])

        print(classification_report(val_target, val_predicts))
        f1 = f1_score(val_target, val_predicts, average='macro')
        print(f1)

        if f1 > self.best_f1:
            model.save_weights('%s-epoch: %d-f1: %.4f-model.hdf5' %
                               (aspect, epoch + 1, f1))
            self.best_f1 = f1


for i, asp in enumerate(aspects):
    aspect = asp
    print('======Fit model for', asp, '======')

    model = BiLSTMWithSimpleAttention(word_index, embedding_matrix,
                                      embedding_size=EMBEDDING_SIZE).build()
    print(model.summary())
    model.compile(loss='categorical_crossentropy', optimizer=Adam(
        lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, amsgrad=True), metrics=['accuracy'])

    hist = model.fit(
        train_sentences,
        y_train[i],
        validation_data=(valid_sentences, y_valid[i]),
        epochs=15,
        batch_size=256,
        shuffle=True,
        verbose=1,
        callbacks=[
            TensorBoard(),
            MetricMacroF1(),
        ]
    )
