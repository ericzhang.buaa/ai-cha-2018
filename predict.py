from keras import backend as K
import pandas as pd
import numpy as np
import os
from common import preprocess as pp
from keras.callbacks import Callback, TensorBoard, ModelCheckpoint
from sklearn.metrics import classification_report, f1_score
from keras.optimizers import Adam
from layers.simple_attention import SimpleAttentionLayer
import tensorflow as tf
from keras.regularizers import l2
from bilstm_with_simple_attention import BiLSTMWithSimpleAttention


# 显示可用GPU信息
print('GPU Info: ', K.tensorflow_backend._get_available_gpus())

EMBEDDING_SIZE = 300
PADDING_SIZE = 500
data_dir = os.path.join(os.path.dirname(__file__), 'data')

testb_data = pd.read_csv('./data/sentiment_analysis_testb.csv', sep=',')
testb_data['content'] = ''
testb_data = testb_data[:1000]
print('TestB set size: ', len(testb_data))

testb_sentences = np.load(os.path.join(data_dir, 'testb_coded_sentences.npy'))
testb_sentences = testb_sentences[:1000]
word_index = np.load(os.path.join(data_dir, 'word_index.npy')).item()
embedding_matrix = np.load(os.path.join(data_dir, 'embedding_matrix.npy'))

aspects = aspects = testb_data.columns[2:]
aspect = None

for i, asp in enumerate(aspects):
    aspect = asp
    print('======Predict for', asp, '======')

    model = BiLSTMWithSimpleAttention(word_index, embedding_matrix,
                                      embedding_size=EMBEDDING_SIZE).build()
    print(model.summary())
    model.compile(loss='categorical_crossentropy', optimizer=Adam(
        lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, amsgrad=True), metrics=['accuracy'])
    model.load_weights('fin/best/' + asp + '.hdf5')

    y_prob = model.predict(testb_sentences)
    y_pred = np.array([y.argmax(axis=-1) for y in y_prob]) - 2

    testb_data[asp] = y_pred

testb_data.to_csv('./predict_testb.csv', sep=',', index=False)
