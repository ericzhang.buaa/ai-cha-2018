from keras.layers import Layer
from keras import backend as K
from keras.regularizers import l2


class ATAE(Layer):
    def __init__(self, d, da, reg=1.0, **kwargs):
        self.d = d
        self.da = da
        self.reg = reg
        super(ATAE, self).__init__(**kwargs)

    def build(self, input_shape):
        self.W_h = self.add_weight(name="W_h_{:s}".format(self.name),
                                   shape=(self.d, self.d),
                                   initializer="glorot_normal",
                                   regularizer=l2(self.reg),
                                   trainable=True)
        self.W_v = self.add_weight(name="W_v_{:s}".format(self.name),
                                   shape=(self.da, self.da),
                                   initializer="glorot_normal",
                                   regularizer=l2(self.reg),
                                   trainable=True)
        self.w = self.add_weight(name="w_{:s}".format(self.name),
                                 shape=(self.da + self.da, 1),
                                 initializer="glorot_normal",
                                 regularizer=l2(self.reg),
                                 trainable=True)
        self.W_p = self.add_weight(name="W_p_{:s}".format(self.name),
                                   shape=(self.d, self.d),
                                   initializer="glorot_normal",
                                   regularizer=l2(self.reg),
                                   trainable=True)
        self.W_x = self.add_weight(name="W_x_{:s}".format(self.name),
                                   shape=(self.d, self.d),
                                   initializer="glorot_normal",
                                   regularizer=l2(self.reg),
                                   trainable=True)

    def call(self, x, mask=None):
        h = x[0]
        a = x[1]

        M1 = K.dot(h, self.W_h)
        M2 = K.dot(a, self.W_v)
        M = K.tanh(K.concatenate([M1, M2], axis=-1))
        alpha = K.softmax(K.squeeze(K.dot(M, self.w), axis=-1), axis=-1)
        r = K.batch_dot(alpha, h)
        h_star = K.tanh(K.dot(r, self.W_p) + K.dot(h[:,-1,:], self.W_x))

        return h_star

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.d)
