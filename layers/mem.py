from keras.layers import Layer
from keras import backend as K
from keras.regularizers import l2


class DeepMemory(Layer):
    def __init__(self, hop=5, n=500, dim=200, reg=0.1, **kwargs):
        self.hop = hop
        self.reg = reg
        self.n = n
        self.dim = dim
        super(DeepMemory, self).__init__(**kwargs)

    def build(self, input_shape):
        self.W_att = self.add_weight(name="W_att_{:s}".format(self.name),
                                   shape=(2 * self.dim, 1),
                                   initializer="glorot_normal",
                                   regularizer=l2(self.reg),
                                   trainable=True)
        self.b_att = self.add_weight(name="b_att_{:s}".format(self.name),
                                   shape=(1,),
                                   initializer="glorot_normal",
                                   regularizer=l2(self.reg),
                                   trainable=True)
        self.D = self.add_weight(name="D_{:s}".format(self.name),
                                 shape=(self.dim, self.dim),
                                 initializer="glorot_normal",
                                 regularizer=l2(self.reg),
                                 trainable=True)

    def call(self, x):
        ctx = x[0]
        asp = x[1]
        loc = x[2]

        # location embedding
        loc = K.repeat_elements(loc, self.dim, axis=-1)
        ctx = ctx * loc

        for k in range(self.hop):
            linear = K.dot(asp, self.D)
            mem = K.concatenate([ctx, K.repeat_elements(asp, self.n - 1, axis=1)], axis=-1)
            att = K.tanh(K.dot(mem, self.W_att) + self.b_att)
            att = K.softmax(att, axis=1)
            attention = K.batch_dot(K.permute_dimensions(att, (0,2,1)), ctx)
            asp = attention + linear

        return K.squeeze(asp, axis=1)

    def compute_output_shape(self, input_shape):
        return (input_shape[0][0], self.dim)
