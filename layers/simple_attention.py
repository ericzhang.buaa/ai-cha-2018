from keras.layers import Layer
from keras import backend as K
from keras.regularizers import l2


class SimpleAttentionLayer(Layer):
    def __init__(self, reg=0.01, **kwargs):
        self.reg = reg
        super(SimpleAttentionLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        self.sentence_length = input_shape[1]
        self.embedding_size = input_shape[-1]

        self.W = self.add_weight(name="W_{:s}".format(self.name),
                                 shape=(self.embedding_size, 1),
                                 initializer="glorot_normal",
                                 regularizer=l2(self.reg),
                                 trainable=True)
        self.b = self.add_weight(name="b_{:s}".format(self.name),
                                 shape=(1,),
                                 initializer="glorot_normal",
                                 regularizer=l2(self.reg),
                                 trainable=True)

    def call(self, x):
        a = K.softmax(K.tanh(K.dot(x, self.W) + self.b), axis=1)
        output = K.batch_dot(K.permute_dimensions(x, (0,2,1)), a)
        output = K.squeeze(output, axis=-1)

        return output

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.embedding_size)
