import jieba
import numpy as np
from keras.preprocessing.sequence import pad_sequences
from gensim.models import Word2Vec
import re
import os


def cut_sentences(sentences, stopwords=[], use_char=False):
    """
    将文本分词
        :param sentences: 文本集合
        :param stopwords=[]: 停用词列表
        :param use_char: 是使用分词还是分字
    """
    word_sentences = []
    for sentence in sentences:
        words = []
        if use_char == True:
            sent = sentence
        else:
            sent = jieba.cut(sentence)
        for word in sent:
            if word not in stopwords and re.match(r'[\u4e00-\u9fff]+|^[\d\w\.%]+$', word) is not None:
                words.append(word)
        word_sentences.append(words)
    return word_sentences


def embed_words(word_sentences, size=100, window=5, min_count=5):
    """
    docstring here
        :param word_sentences: 分词后的文本集合
        :param size=100: Embeding向量维数
        :param window=5: Context长度
        :param min_count=5: 最少出现次数 
    """
    model = Word2Vec(word_sentences, size=size,
                     window=window, min_count=min_count)

    word_index = {}
    i = 0
    for w in model.wv.vocab:
        i += 1
        word_index[w] = i

    embedding_matrix = np.zeros((len(word_index) + 1, size))
    for word in word_index:
        if word in model.wv.vocab:
            embedding_matrix[word_index[word]] = model.wv[word]

    return word_index, embedding_matrix


def code_sentences(word_sentences, word_index, null_code=0, padding=True, padding_max_length=300, padding_position='pre', truncating_position='post'):
    """
    将分词后的文本编码
        :param word_sentences: 分词后文本
        :param word_index: 词表
        :param null_code=0: 若词不存在于词表，默认编码
        :param padding=True: 是否padding
        :param padding_max_length=300: padding的最大截断长度，None表示不截断，取所有文本中最大值
        :param padding_position='pre': 从前面padding还是后面padding
        :param truncating_position='post': 从前面截断还是后面截断
    """
    coded = [[word_index.get(word, null_code) for word in sent]
             for sent in word_sentences]
    if padding == True:
        coded = pad_sequences(coded, maxlen=padding_max_length,
                              padding=padding_position, truncating=truncating_position)
    return coded


def encode_one_hot_label(label, class_num):
    """
    将类别向量编码为onehot
        :param label: 类别向量，非负整数，每个不同的数字表示一个类别
        :param class_num: 类别数目
    """
    y = np.zeros((len(label), class_num), dtype='int32')
    i = 0
    for x in label:
        y[i][int(x)] = 1
        i += 1
    return y


def decode_one_hot_label(y):
    """
    将onehot编码label变回整型label
        :param y: onehot数组
    """
    label = np.zeros(len(y), dtype='int32')
    for i in range(len(y)):
        for j in range(len(y[0])):
            if y[i][j] == 1:
                label[i] = int(j)
                break
    return label


def target_term(aspect_name, word_sentences, padding_size):
    file_path = os.path.join(os.path.dirname(
        __file__), '..', 'data', 'aspect_words_' + aspect_name + '.txt')
    targets = []
    with open(file_path, 'r') as f:
        for line in f:
            line = line.strip()
            targets.append(line)
    locations = []
    for sent in word_sentences:
        find = False
        for target in targets:
            for loc, w in enumerate(sent[:padding_size]):
                if w.startswith(target) or w.endswith(target):
                    if len(sent) >= padding_size:
                        locations.append(loc)
                    else:
                        locations.append(loc + (padding_size - len(sent)))
                    find = True
                    break
            if find == True:
                break
        if find == False:
            locations.append(-1)
    return locations


def compute_location_vector(loc, padding_size):
    vec = np.zeros((padding_size, 1))
    for i in range(padding_size):
        vec[i, 0] = 1. - abs(i - loc) / padding_size
    vec = np.delete(vec, loc, axis=0)

    return vec
