import os
import pandas as pd
from common import preprocess as pp
import numpy as np

data_dir = os.path.join(os.path.dirname(__file__), 'data')

# 常量
EMBEDDING_SIZE = 300
PADDING_SIZE = 500
USE_CHAR = True

# 读入数据
train_data = pd.read_csv(os.path.join(
    data_dir, 'sentiment_analysis_trainingset.csv'), sep=',')
train_data['content'] = train_data['content'].str.strip('"')
print('Train set size: ', len(train_data))

valid_data = pd.read_csv(os.path.join(
    data_dir, 'sentiment_analysis_validationset.csv'), sep=',')
valid_data['content'] = valid_data['content'].str.strip('"')
print('Validation set size: ', len(valid_data))

testa_data = pd.read_csv(os.path.join(
    data_dir, 'sentiment_analysis_testa.csv'), sep=',')
testa_data['content'] = testa_data['content'].str.strip('"')
print('TestA set size: ', len(testa_data))

testb_data = pd.read_csv(os.path.join(
    data_dir, 'sentiment_analysis_testb.csv'), sep=',')
testb_data['content'] = testb_data['content'].str.strip('"')
print('TestB set size: ', len(testb_data))

print('======Load data completed======')

# Stopwords
stopwords = []
with open('./data/stopwords.txt', 'r') as f:
    for w in f:
        w = w.strip()
        stopwords.append(w)
stopwords.append('\n')
stopwords.append(' ')

# 分词
train_word_sentences = pp.cut_sentences(
    train_data['content'], stopwords=stopwords, use_char=USE_CHAR)
valid_word_sentences = pp.cut_sentences(
    valid_data['content'], stopwords=stopwords, use_char=USE_CHAR)
testa_word_sentences = pp.cut_sentences(
    testa_data['content'], stopwords=stopwords, use_char=USE_CHAR)
testb_word_sentences = pp.cut_sentences(
    testb_data['content'], stopwords=stopwords, use_char=USE_CHAR)

print('======Word segmentation completed======')

# 使用训练数据做word embeding
word_index, embedding_matrix = pp.embed_words(
    np.concatenate([train_word_sentences, valid_word_sentences, testa_word_sentences], axis=0), size=EMBEDDING_SIZE, window=10)

print('======Word embedding completed======')

# 对分词后文本进行编码和padding
train_coded_sentences = pp.code_sentences(
    train_word_sentences, word_index, padding=True, padding_max_length=PADDING_SIZE)
valid_coded_sentences = pp.code_sentences(
    valid_word_sentences, word_index, padding=True, padding_max_length=PADDING_SIZE)
testa_coded_sentences = pp.code_sentences(
    testa_word_sentences, word_index, padding=True, padding_max_length=PADDING_SIZE)
testb_coded_sentences = pp.code_sentences(
    testb_word_sentences, word_index, padding=True, padding_max_length=PADDING_SIZE)

print('======Code and padding completed======')

aspects = aspects = train_data.columns[2:]

y_train = [pp.encode_one_hot_label(
    train_data[cla_name] + 2, 4) for cla_name in aspects]
y_valid = [pp.encode_one_hot_label(
    valid_data[cla_name] + 2, 4) for cla_name in aspects]


# 保存
np.save(os.path.join(data_dir, 'train_word_sentences.npy'), train_word_sentences)
np.save(os.path.join(data_dir, 'valid_word_sentences.npy'), valid_word_sentences)
np.save(os.path.join(data_dir, 'testa_word_sentences.npy'), testa_word_sentences)
np.save(os.path.join(data_dir, 'testb_word_sentences.npy'), testb_word_sentences)

np.save(os.path.join(data_dir, 'train_coded_sentences.npy'), train_coded_sentences)
np.save(os.path.join(data_dir, 'valid_coded_sentences.npy'), valid_coded_sentences)
np.save(os.path.join(data_dir, 'testa_coded_sentences.npy'), testa_coded_sentences)
np.save(os.path.join(data_dir, 'testb_coded_sentences.npy'), testb_coded_sentences)

np.save(os.path.join(data_dir, 'word_index.npy'), word_index)
np.save(os.path.join(data_dir, 'embedding_matrix.npy'), embedding_matrix)

np.save(os.path.join(data_dir, 'y_train.npy'), y_train)
np.save(os.path.join(data_dir, 'y_valid.npy'), y_valid)
